var gulp = require('gulp');
var clean = require('gulp-clean');
var rename = require('gulp-rename');

// Remove current .htaccess & wp-config.php
gulp.task('clean', function() {
    return gulp.src(['.htaccess', 'wp-config.php'])
        .pipe(clean());
});

// Set the environment to development
gulp.task('dev', ['clean'], function() {
    var stream = gulp.src('env/.htaccess.dev')
        .pipe(rename('.htaccess'))
        .pipe(gulp.dest('./'));

    gulp.src('env/wp-config.php.dev')
        .pipe(rename('wp-config.php'))
        .pipe(gulp.dest('./'));

    return stream;
});

// Set the environment to production 
gulp.task('prod', ['clean'], function() {
    var stream = gulp.src('env/.htaccess.prod')
        .pipe(rename('.htaccess'))
        .pipe(gulp.dest('./'));

    gulp.src('env/wp-config.php.prod')
        .pipe(rename('wp-config.php'))
        .pipe(gulp.dest('./'));

    return stream;
});

// The default task
gulp.task('default', ['dev']);