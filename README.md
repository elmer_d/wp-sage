# wp-sage

wp-sage is a WordPress boilerplate that uses Sage as a starter theme.

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| composer >= 1.3    | `composer`     | [getcomposer.org](https://getcomposer.org/download/) |
| Node.js >= 4.5  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| Bower >= 1.3.12 | `bower -v`   | `npm install -g bower` |

## Features

* install wordpress via compser
* has a modified wordpress file structure for lightweight repo storage
* looks for images on the production if not present on local machine via .htaccess
* switch debugging mode between development and production via wp-config.php
* build automation and deployment via Bitbucket Pipelines and git-ftp

## Installation

+ Make sure [Git](https://git-scm.com/) has been installed before moving on.

      Clone this repo on your local machine.

      `$ git clone https://elmer_d@bitbucket.org/elmer_d/wp-sage.git your-site-directory`

+ Go to your-site-directory

      `$ cd your-site-directory`

      Your file structure should look like this

```
    your-site-directory
    │── env
    │   │── .haccess.dev
    │   │── .haccess.prod
    │   │── wp-config.php.dev
    │   └── wp-config.php.prod
    │ 
    │── plugins
    │   └── index.php
    │ 
    │── themes
    │   └── index.php
    │ 
    │── .git-ftp-include
    │── .gitignore
    │── composer.json
    │── gulpfile.js
    │── package.json
    │── README.md
    └── wp-config.php.dev
```



+ Make sure [Composer](https://getcomposer.org/download/) has been installed before moving on.

    Install Wordpress and Sage using Composer.

```
# @ your-site-directory/
$ composer install
```
     
     
   This should generate these directories


   
```
your-site-directory
...
│ 
│── themes
│   └── sage                                    # Sage Theme
│   
...
│── vendor                                      # Composer Packages (don't mind this)
└── wp                                          # Wordpress Core
```



+ Go to the **env** directory and configure **.htaccess.dev**
Replace **{PROD}** with your production site URL (e.g. `http://example.com/uploads/$1 [NC,L]`)
   
      This will allow your **local Wordpress site** to look for images on the **production site** if they are not on your local machine.


```
<IfModule mod_rewrite.c>
  RewriteEngine on
    
  # Attempt to load files from production if
  # they're not in our local version
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule uploads/(.*) \
  http://{PROD}/uploads/$1 [NC,L]
</IfModule>
```
   
   > **Note: If you still don't have a URL for your production site just leave this as is**



+ Configure your database via **wp-config.php.dev** and **wp-config.php.prod**

    * **wp-config.php.dev** - for your local and/or staging database setup
    * **wp-config.php.prod** - for your production database setup

```
define('DB_NAME', 'database_name_here');
    
define('DB_USER', 'username_here');
    
define('DB_PASSWORD', 'password_here');
    
define('DB_HOST', 'localhost');
    
define('DB_CHARSET', 'utf8');
    
define('DB_COLLATE', '');
```
   
   > **Note: If you haven't got the database set up on your production site just leave this as is**



+ Generate your **wp-config.php** via **gulp**
```
# @ your-site-directory/
$ npm install
$ gulp
```

   
   or


```
# @ your-site-directory/
$ npm install
$ gulp dev
```

   
   You should now have a **wp-config.php** on the root of your-site-directory


```
your-site-directory
...
└── wp-config.php                       #This is generated from env/wp-config.php.dev
```

## Sage installation and development

* Please see the documentation on [github.com/roots/sage](https://github.com/roots/sage)

## Deployment
You will be using deploying your site via Bitbucket Pipelines using git-ftp

+ Create a repository on Bitbucket
You can see an example [here](https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html).

+ Register your remote url on your local repo
`$ git remote add bitbucket https://example@bitbucket.org/example/your-repo.git`

+ Add your files
`$ git add .`

+ Commit the changes on your files
`$ git commit -m "your message here"`

+ Push your repo to your remote on Bitbucket
`$ git push bitbucket master`

+ Go to Bitbucket, open your repository and create a pipeline
Click [here](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) to learn how to create a Bitbucket Pipeline

+ Once you will be redirected to page where you will create your own **bitbucket-pipelines.yml** file copy the code below:
```
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: elmercd/dev-sage

pipelines:
  branches: # Pipeline is triggered when there is a commit on master branch
    master:
      - step:
          script: # Modify the commands below to build your repository.
            - npm install
            - composer install
            - gulp prod --allow-root
            - cd themes/sage/ && ls -al
            - npm install
            - bower install --allow-root
            - gulp --production --allow-root
            - cd ../..  && ls -al
            - git status
            - git ftp init --user $FTP_USERNAME --passwd $FTP_PASSWORD $FTP_SITE
            - echo "Build Successful!"
  custom: # Pipeline is triggered for custom actions
    test:
      - step:
          script: # Modify the commands below to build your repository.
            - npm install
            - composer install
            - gulp prod --allow-root
            - cd themes/sage/ && ls -al
            - npm install
            - bower install --allow-root
            - gulp --production --allow-root
            - cd ../..  && ls -al
            - git status
            - git ftp init --user $FTP_DEV_USERNAME --passwd $FTP_DEV_PASSWORD $FTP_DEV_SITE
            - echo "Test Build Successful!"
```

+ Before clicking **COMMIT** open a new browswer tab and open **Settings > Environment variables**

+ Input the values for the following:

      * **$FTP_USERNAME** - the ftp username

      * **$FTP_PASSWORD** - the ftp password

      * **$FTP_SITE** - the root of your site (e.g. ftp://example.com/public_html/)

      * **$FTP_DEV_USERNAME** - the ftp username for staging

      * **$FTP_DEV_PASSWORD** - the ftp password for staging

      * **$FTP_DEV_SITE** - the root of your staging site (e.g. ftp://example.com/public_html/)

+ Go back to the page where you are creating the **bitbucket-pipelines.yml** file and press **COMMIT**

    This will redirect you the build process.

    Once the build is successful check your site.

+ On your local machine pull the changes made on your remote repo so that your local machine will have the **bitbucket-pipelines.yml** file

+ Update your code locally.

+ Edit `git-ftp init` to `git-ftp push` on your